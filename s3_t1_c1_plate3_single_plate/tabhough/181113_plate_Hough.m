(* ::Package:: *)

ttot=53;ztot=101;xyp=0.1088;zp=0.25;thresholdBrs=0.6;
n=8;(*select every n-th points along either x and y*)

If[FileExistsQ["./pts_n"<>ToString[n]<>"tbrs"<>ToString[thresholdBrs]<>".m"],

Print["import pt"];pt=Import["./pts_n"<>ToString[n]<>"tbrs"<>ToString[thresholdBrs]<>".m"];,

Print["loading image"];
rs=Image[Rescale[ImageData@#,{Min[ImageData@#],Max[ImageData@#]},{0,1}][[;;;;n,;;;;n]]]&/@Import["../hyperstack_plate3.TIF"];(*Rescaled stack*)
brs=Table[Binarize[rs[[i]],thresholdBrs],{i,Length[rs]}];(*binarized rs*)
zs=20;ze=96;
Print["calc. pt"];
pt=Table[Flatten[Table[Insert[z*zp,-1]/@(PixelValuePositions[brs[[(t-1)*ztot+z]],1]*xyp*n),{z,zs,ze,1}],1],{t,1,ttot,1}];

Export["./pts_n"<>ToString[n]<>"tbrs"<>ToString[thresholdBrs]<>".m", pt];]

pi=3.1415926;
Do[
XYZ=pt[[t]];
rMax=Max[Table[ Norm[XYZ[[i]]],{i,Length@XYZ}]];
spaceResolution=0.108*n;degreeResolution=1;
nr=Round[rMax*2/spaceResolution,2]; nth=IntegerPart[180/degreeResolution];nphi=IntegerPart[180/degreeResolution];
dr=rMax/(nr/2);
dth=pi/nth;
dphi=pi/nphi;
tabHough=Table[0,{i,nr},{j,nth},{k,nphi}];
Print["t ", t];
Do[tabHough[[nr/2+Quotient[XYZ[[nn,1]] Cos[k*dphi-pi/2] Cos[j*dth]+XYZ[[nn,2]] Cos[k*dphi-pi/2]Sin[j*dth]+XYZ[[nn,3]] Sin[k*dphi-pi/2],dr]+1,j,k]]++,
{nn,Length[XYZ]},{j,nth},{k,nphi}
];
Export["./Hough_xyz_t"<>ToString[t]<>".m", XYZ];
Export["./Hough_tab_t"<>ToString[t]<>".m", tabHough];,
{t,51,53,1}
]
Print["Done"];
