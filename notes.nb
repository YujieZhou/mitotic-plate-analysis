(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     11054,        240]
NotebookOptionsPosition[     10363,        222]
NotebookOutlinePosition[     10720,        238]
CellTagsIndexPosition[     10677,        235]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"rawimg", "=", 
   RowBox[{"Import", "[", 
    RowBox[{"FileNameJoin", "[", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"NotebookDirectory", "[", "]"}], ",", 
       "\"\<s3_c0_MN1 34-39\>\"", ",", "\"\<MN1 34-39.tif\>\""}], "}"}], 
     "]"}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7520633511945887`*^9, 3.752063354635202*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"bd616b0b-e881-4a29-87e2-30c59788343c"],

Cell[BoxData[
 RowBox[{
  RowBox[{"imgtab", "=", 
   RowBox[{"ImageData", "/@", "rawimg"}]}], ";"}]], "Input",
 CellChangeTimes->{3.752063368586474*^9},
 CellLabel->"In[2]:=",ExpressionUUID->"a064015f-60b0-4f2e-a15f-b44d990cef00"],

Cell[BoxData[
 RowBox[{"ListPlot", "[", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Max", "[", 
       RowBox[{
       "imgtab", "\[LeftDoubleBracket]", "i", "\[RightDoubleBracket]"}], 
       "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "1", ",", "606"}], "}"}]}], "]"}], ",", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Min", "[", 
       RowBox[{
       "imgtab", "\[LeftDoubleBracket]", "i", "\[RightDoubleBracket]"}], 
       "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "1", ",", "606"}], "}"}]}], "]"}]}], "}"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.752063049830804*^9, 3.75206316272887*^9}, {
  3.752063197156107*^9, 3.752063207425149*^9}},
 CellLabel->"In[20]:=",ExpressionUUID->"29f2bddd-72e4-42cb-8b4b-88762a59851b"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.004583333333333334], 
     AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxdWjmM3MgVJRwpZJ+alTQSbRiGYBhG+5ZPfa8veU9qJa1GN+e+Z9j3Nd1T
ZKSwQ4UMFXaokKHCDhUyVNjhhv7V+gXj/XmABk+/LpJV79Wvmh9vnX+3+yPP
817yP/b3p5/l3UpeZmzdlf+gUmCxKfyK4j6VPQtXvkrlrMRw8TVV/gaVIgvH
A1X/J6r+T6kSWGwL/xmVcp8RCb+t6v9c9fcLwuf5pSrfoLIpMVz5X6nx/Zoq
kYXr/zdULSoMx39LdVNj7Ar/HdVWcPz3dC37jHEo/A90c4Uz4XfIX/3Ewv8o
/TeF/0nG2xL+Zyqv4PhfqGoqjLbwvyr+N6rEFo7fpdqyyugIJ6p7Fr1P3JA8
T1fif5fnEW6YF1WGq/85VX0L4eZz1f8/5P0JN8yLMsON/59UmZUZwg3zhoWr
/y+qBhaufeZ5heHi/6ZqbOHizGcVhov/B8sb5g0LN/57VI0sXP17VDNVhov/
V42f+Qqu/S/U839BtXmV4ep/ie/DfEm1hoV7v1/heMxXVPEtXPtfUyUrM9z7
+VrWg4t/o8b3DX4v71vV/7dqPoRU9SykfwqxfRNSfVZjSHt5qJ7/Pr5vui/r
zY33Ps7XnOOehav/HY6HvqNyYOHqc3xRZrj6HG9YuPnzgKpZheHqP6ByaOHq
K54/wPfpPcTx0EMqRxauPvNliSHrMX+o1t8jqpgyQzg9ovK8xJDyhnlRYrj6
j7B973sqz0oMiRPzRYnh6n9PpdjC1ed4XmI4fXiMcXpMfmwhemIeU1DcYpxL
/cd0M1tnnEr9DbruWRxL/Q26FlkcSX3mKwjPN0Qv9qT+E6rlVcaO1H8i80P0
0TxBPc6fKP19Ku9H4vSUSpnPEH03zI3PEJ4/Vfr8jEqFz5A4PcP+zDMcT67i
3nOZbzIeei7zwY3/OZU8Czf+52r8L9Cf6AX5kYUb/wvRS3k/uSrvvZT54/p/
iX5lXlItqzJc/Zd0Nasz3Pt/Rdfzawz5PvSK1vMbjBOp/4pKvoWbP69wPXgR
VecVhsz/IML1SxHVAgtZ/1GE68VwPLaQeMY8tHB6we2HFqIvhYp7mzJ/JB5s
yvM6PdpEPY42qbqsMFz/m6i/2SbqYb6J+lJsKv3aoppnIeMJtpSebeH4Iy6/
qDKcXqt4toX6mytebOH78rZx/ME2tkfbVF/UGO79c3wF1/82jifbRj3Mt/H7
FtxeYCF+7+2gXwU76vk57lu4/neUn+7gfMg4vqgwXP876CcF83mZ4ebfLvpP
sIv+R7v4PaNdrG92sf1sV+n9LuptweWXZYbrfw/3G8Ee7rdoD/0o2lP+sofP
m+3h+8n30G+KPfX8+7jegn3lh/tYPtpX/e+jH2X76Cf5Pj5PsY/+4h1QaQXh
wQH6HR3Iflv8JDoQvXX+wnzmM4Rnqr38gAJziyH+UxzQzcDC7X8PaT2yED8K
Dulmvs4QThwPLIRHh0ofD+X7Cs8O5X0Izw9R34tDtd8/Qj8IjsRfnR8doT5H
R6r/I8xHsiPyV3B+dYT5TKG4d4z+EhxjnI7RL6Jj5Y/HOL7sGP0xP0a/Ko6p
Hlk4/zjB/CQ4oRv5dYbbD5xQqWEh3zM6obJv4ebfiZr/J1QJLdz8P0G9L05Q
b71T1K/gFPWeTqm+rDGc/pyiH5lTzA+yU6U/p6gvxanKP85QD4Iztf7OUB+i
M8x/jOKZ4vkZ5lvFGeqZd47PF5zjfp3O0R+ic/QHc456n52jP+Tn+D4K7i+r
Mdz7j6nuW4gf+DGOP4jRnxsxjodi7D+MMZ+JYvTrOFb+FeP3nqn6mep/rsrn
MeZTC9VfEaM/LWOVPzXxe/tNjAdNnB+NJs4HaqI+h03cX0RNnG9xU/mnKj9T
PFPjmTeVvzUxf140ld801fM3cf16LeR+S60HjscW7vlbKt9R8bCF/hKp8nEL
8xXTkvMU9/wt5WctPA+Zq3jewudbtPD7FC3c/y5bdCuyED/y2pg/+G3U/6CN
etxoo79QG/U0bKMfRG35/lI+biv9VvFZW9a/6H3WRv2et5W+q/qLNuZLRRv9
banKex30D7+j/Kijnr+D/kkdWc+Sn4Qd0Wc5/4o6tBZZHMjzd9T37+D3nXVw
PWcdXJ/zDup93kF9XXTQT4oO7l+XHeU/XYz7zGdVhtO/Ls7PRhf1iLq4nsMu
+k2k2ou7Kn/oon7OungelXXxeedd5a8qvujieihUfNlV52s99Bu/p/IRFW/0
lD+r8mEP9TfqoZ7FPRy/6eH4Zqp+1sP3O++h/+U9fN8LVb7oof8tFff6OB6/
j/Mt6KP+Nvo4PlLxsI/tR33Mv+K+yp/7Sv/7uD/I+pgfzPuoz7niiz76UaH4
sq/Orwao3/4A30cwwPfZGKjzgQH6UajaiwZK/wfYvxlgvjFT8UzF56r9fCD6
JvHFQPRKeDHA/GQ5EH105+1DPB/zh3SzWGe4fGRINzwLOU9pKE5DPB8Lh5L/
iD5GQ8wX4iHquRmKXzv9H6JeZ0P0i/lQnUcNMT9YDNHPClV+qeLeCP3EH6E/
BCOl/yPlf6p+qHg0Qj+NVX0zwvuT2Yg+y9cY4hfZiNY9C/ke8xHmj/kI58ti
pPx/hPq9HCn9H+P5tj/G/XEwxvuRxhj3y8Q8thAejunqrM4QHo3x/Cfm8g0L
d98yRj+ZjTEfyFR8PkY/ycfoF4sx+mcxRn9cjtHvvAvc7/sXeP8TXKA+Ny5w
f06qfqjKRyoeX6j7GlV+puLZhdL/C5XfXKBeLy7w/Kq4QH9YXqj7mQnqtz/B
/XQwQb1uTFCfaYL+GE5UvjhR+jdR5zcqPpvgfUc2wfuO+QTPo/IJ6uNCxYsJ
ro/lBPXTm2L//hTPi4Ip5geNKe7vaYr9hVPZH7r9/xTHF6vyZop6PVP9Z1M8
f5pPKchvMdx9hoovpnRrBXfeNMXzpOWUrmfXGKLX3iXqt39JV02dIfv54BLv
OxqX6rz/EvU8vES9jy5RD+NL1Huj+OxSneeo+vNLvF/IVf3FpTpvUnx5qe4/
DOYrVwz6l2/wPmTN4PgCg/5y22D7DYP5wh2j7lsM3v/cMzje0OD9+4YRvZN8
IzJ0NbDY/8QPDOZjsaH17AZD/HpgyG9YuPNLg+vrtVF6ZNAP3hilz9z/ss4Q
PX9rZP4InzMPLfqf+DuD/pFzPLaQ+HuO5zWGO88wtOZbDD7xD0buf6R+wXxe
Zwj/qPhStf8Dt7+CO/9P8D7gSoJ+5yfoV2sJnr8FCY7ndoJ+10jQb+9we/Ma
QzgleN9zL8HnDxP0u40Ev0eUYD5wkKC+xwn6ySBR9/0J3q+8TtA/Z6r/N4k6
70vQT98m6E/zBOfTuwTzpTzB/O99gvnAQvEPCe73iwT3Nx8T9J+lav8Hxb0U
27+Sol/6Kfa3lqJ+Byp+W/FGivfVd1Lcj1OK+f+9FM+bwxT/PmYjxfuEKKUb
K8j6PmBurjPk/DpO8fxhkKJemJRqkYXsP1+nqC+zFPfDb1LU9yxFP3ibEvx9
0DzF/e67FPU0T1G/36eor4sU/ehDqs7z1fg+pmq////+/weYVs4W
      "]]}, 
    {RGBColor[0.880722, 0.611041, 0.142051], PointSize[0.004583333333333334], 
     AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxdmTlwHMcVhrsUMVwctECbEkculYvlcrnWlm1RPsSWLIqgeC0AHuAFDu4b
mMXeJ3o3YrghwwkZbsiwQ4UbMpyQ4YYKPQvNuur/+KrIenzd/f539uvZLzZO
l7c/McaspX9N/v31z/jmvJtLaeVm9h921kxoOeMv2Xk/l9JUnrPzF/Qo4+ex
fgH8VTvjcikVMj6A/Pf2M3c1pXsZ/yX0/cFe9vMpTfVdt3MXNJX/EfI/2Vk3
k9JSxv/ZzvqZlKZ83s6ZCU33/wX2/NUG7lpKDzP+KzvjcylN8f8N6/9u582E
pvw/cP7XsOcG9n8De/5pr5oJTf3xL8j/DX3/wfnf2twFTfHehD6r/nIW8u/s
ZTef0lT+Hfz3vZV8cd9D/3+Vd+DND4rf/YB8uGXn/GxKGe9uQf6j/Z2Z0N1M
/iPkt9V+d1vxmkW11y2qveaOveIWUlrM5Hfg7580Hu4n+Puu5p+7a+fcbEpT
fPfg/3uI7334/z72P7AzFzQ9/wHkD+2MmdBU/tDmzISm+AqK1xYQz4K9bCZ0
61feY71Z0vy2S6rfLal9fgn7l9U+u6z15pY1X/wy4rei/rIrwL+i5/sV9Yd5
pPG3jzS/HeT+EfLjMfaDd481X/xjO3tB0/g8UTz2ifYX9wT4n2i8zVP1j32K
fHuq/vHgzSrit4p6XNV+7VeB/xn0P0O+PrO/cZdTmubPM+h/Dv3P7RUzoTvZ
/ueqzz9HP36h+OwL5PsLPd+/QPxeQv9LxO8l4vcS+fcK+l9p/3GvNB7+Ffy3
pvVi19Av17B/Df35teKzr9Ue9xr5C7kJ1Z4gRD2GGq8wVPwu1H4Sh2qPB59A
n1lXebCu/cKuqz/DdfTzdXvNTCjLh3hd7wu/rvgS6DMb6r9gA/1kQ/0XbiC+
G5pvMdZ7yJMNzCeb0L+JfIQ83MR9s6nnxZD7TcWbgDdbqi/YQvy3YP8W4o/1
8ZbG128pngTnmW3N9wC83Vb/hZC7bc2HeFv7p9/W+km2s/vscaZ/R+XBDupp
R/0b7uB+gzzeQfx3FG+C9WZX1we7yL9d9Xe4i3loV8+Pd1F/uxrfZBf1twf/
78F+yMM95P+e4on3cF/twf/gzT76z772d7uv+MN96N/X8+J9+H9f78sE55sD
9W9wYK+5z1N6kOk/QP87wP12oP6Osd4fIP4HmD8O1Z7gUP1hD2H/ofYzh/3x
odaDP9R5JjmE/48Ub3CEeeRI8YRHqL8jzZf4SOvLHyH/juzn7rOU7mf6j7V/
B8fo/8d2wX2a0u1M/zH0Hyu++Fj94Y/Rf481X80J6v8E/edE31/hCeof6+MT
xP9E8zHBenOqeIJT2H+K+jvVecOd6vnxqea3P8X9e4r3a6T7cxH6QaT25CG3
kdZDIdL4hlgfRXjvRJovgwj5HGn+DCONp8d5I+BJIs3PMewxRdhfhP1Fza88
eFuE/UX0i6J+z4iw3xUV36CI+wz4hsDnsX9UzOrpZmY/1o+L2t/MmcYrdwb7
z7R/5CG3ZxqvwhnuK6yPzjBfQz44Qz85U38Moc+faf2MgDfB+WPoNyU9L1fS
/ApKWj95rLcl/f5QKOl9E5Y03lEJ81tJ8Q9wflxSfwyBx5e0f41KWk9JSeMx
LmH+KCueXBnxL+v5echtGfEvY17E/qiM+b2s+AZl7W8x8A2h35eR/9CXlNX/
4zLyv6L1m6vA/orWb76C9xrWFyq4Lyvav6IK7K+ofQPwcQXxr+A9XNF8HVUw
78O+MfCYKuyvYh6FPA/eYn2hqv4Jq2pPVMX7AfsH2B9XUf9V3G9V9P+qzhNJ
FfEHb2qKJ1fDPFRTffka7uca8h98WEP8a5jfa9ovBjXc5+CHNeQ/8I6gLwHe
cQ3fP+qaX7k68r+u+/N1zOd1xL8O++vq36gO++sa7wHkMc4fAp+HvhH0JXX1
z7iO+aeB/t/Ae7yB+De0v9qG+rvQwP3fUP9FWO8ael8NGnhPA8+wgfdNQ+0Z
NfDegL4xeNPU+yvXVDxBU/Mz39R6tE37qZlQNp8XmrC/ifu/ifmvqf1+0NT4
xU3Ev4nvC03EH3zSRP+DftNC/bfwPaSF+Re8bWn/LbTwvaCl+KMW3g/YP2gh
/1vqvyHO9y3NzxHWJ9A/hr2mjfm3rf4K2qj/tr7HbFvv20Ib7xWcH7Ux/0I+
aOP+g3zYxvfcNuxv473T1voYQ246uP86mt9BR/XlO1oPtqN4Cx3Ev4P5D3LX
wfwLPob+YUfzw0P/CPqSDuY/yE0X9ne1foIu5l/IbVfrt9BVfGEX8YfcdTH/
dGE/5EPIPeSjrv2tu5JS9vtBAvzjLvrfOfL/HN8DzlVf/lzxW+wvgA/P1T/R
Ofr/uebDIOUvKLtf4nON1xB4/Lnm4wj6E9gzxnnG6f1yyWm95Jz6d8Hhex3k
1x3qxWm+3HD6PcU6zc9F4Ck4zc9VB//i/D3II6ffy+rQ52DPG4d4wJ63Tvt1
7DQe74B3CHveA68Hvp+xfwR8H+DvxGk+fgTeMfD94jRfTE/r41JP9+d66s8F
8EFPz7/eU/vyPbX/Rg/3Z0/9vQh5Aeevgg97mq97PcybPfVnvYff86HvTU/r
ZQB73wJvDHvf4bxhT+P3Hvg99P3c0/lyhHh8gD0JzvsI/WPg+6Wn+WL6at+l
Pu6DPuq/j/dBX/1xvY/7Aufd6OP3BZy/2Mc81dfvU6vAGwLPHs6L+mpvva/5
7/rqnzfAN4B9b3FeDPm7vs4jQ9j/Hvp9H/GH/0aw9wPWJ8DzEf4Y9/9fD/8D
N8EXgw==
      "]]}, {}}, {}, {}, {}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0., 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{"CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{0., 606.}, {0, 0.005874723430228122}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Input",
 CellChangeTimes->{3.7521176327007513`*^9},
 CellLabel->"In[21]:=",ExpressionUUID->"499801d5-c4b0-461b-97d9-025393b15eb7"],

Cell["\<\
The above figure shows the max and min pixel brightness of each image in the \
(t*z=6*101)  hyperstack.
Therefore, one needs to Rescale[] across the whole stack, rather than image \
by image, to suppress noise in those totally dark images.\
\>", "Text",
 CellChangeTimes->{{3.752063406530208*^9, 3.752063581228979*^9}, {
  3.752117637420002*^9, 3.752117681074677*^9}},
 FontColor->RGBColor[
  1, 0, 0],ExpressionUUID->"40ed94de-4409-469b-8b9a-5e49d23a1787"]
},
WindowSize->{1279, 663},
WindowMargins->{{Automatic, 0}, {1, Automatic}},
CellContext->Notebook,
FrontEndVersion->"11.3 for Linux x86 (64-bit) (March 12, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 456, 11, 31, "Input",ExpressionUUID->"bd616b0b-e881-4a29-87e2-30c59788343c"],
Cell[1017, 33, 230, 5, 31, "Input",ExpressionUUID->"a064015f-60b0-4f2e-a15f-b44d990cef00"],
Cell[1250, 40, 812, 23, 31, "Input",ExpressionUUID->"29f2bddd-72e4-42cb-8b4b-88762a59851b"],
Cell[2065, 65, 7824, 144, 232, InheritFromParent,ExpressionUUID->"499801d5-c4b0-461b-97d9-025393b15eb7"],
Cell[9892, 211, 467, 9, 60, "Text",ExpressionUUID->"40ed94de-4409-469b-8b9a-5e49d23a1787"]
}
]
*)

